﻿
# [](https://gitlab.com/dlsconsultoria/desafiodbserver)Automation Practice - Automação de Teste de Software


**Softwares necessários:**
IDE: [Eclipse](http://www.eclipse.org/)
Java: [Java](http://www.oracle.com/technetwork/pt/java/index.html)
Mozilla Firefox: [Mozilla](https://www.mozilla.org/pt-BR/firefox/new/)


**Passos para a execução do script :**
 1. Baixe o projeto e em seguida descompacte em uma pasta local;
 2. Acesse o Eclipse;
 3. Dentro do Eclipse clique em "File" >>> "Open Projects from File System...";
 4. Em "Import Source", digite o endereço do projeto. Exemplo: "C:\Projetos\automation-practice". Ou clique em "Directory" e 		selecione a pasta do projeto "automation-practice" e clique em "OK";
 5. Clique em "Finish" e espere o projeto carregar;
 6. Após, clique em "src/test/java";
 7. Clique no pacote "testsuites" e com o botão direito sobre o regressionTestSuite.java e selecione "Run As" e após "JUnit Test";
 8. Na primeira vez de excução o teste irá falhar pois precisa atualizar o navegador para o teste, aguarde a atualização;
 9. Após terminada execute novamente o teste;
 10. Espere o caso de teste ser executado no navegador web;
 11. Ao terminar e a barra de status do JUnit ficar verde, acesse a pasta do projeto e clique em "reports" e clique para abrir o report.html no navegador web, você poderá verificar o resultado gerado.
 
**Caso de Teste :**

Realizar uma compra com sucesso
1. Acessar o site: www.automationpractice.com.
2. Escolha um produto qualquer na loja.
3. Adicione o produto escolhido ao carrinho.
4. Prossiga para o checkout.
5. Valide se o produto foi corretamente adicionado ao carrinho e prossiga caso esteja tudo certo.
6. Realize o cadastro do cliente preenchendo todos os campos obrigatórios dos formulários.
7. Valide se o endereço está correto e prossiga.
8. Aceite os termos de serviço e prossiga.
9. Valide o valor total da compra.
10. Selecione um método de pagamento e prossiga.
11. Confirme a compra e valide se foi finalizada com sucesso.