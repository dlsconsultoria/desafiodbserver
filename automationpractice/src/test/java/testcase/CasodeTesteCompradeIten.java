package testcase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.Status;
import framework.drivers;
import framework.report;
import framework.screenshot;
import tasks.AdicionaItemaoCarrinhoTask;
import tasks.EnderecoTask;
import tasks.NovoUsuarioTask;
import tasks.PagamentoTask;
import tasks.EnvioTask;
import verificationpoints.verificationPoint;

public class CasodeTesteCompradeIten {
	private WebDriver driver;
	private AdicionaItemaoCarrinhoTask add;
	private EnderecoTask address;
	private NovoUsuarioTask newUser;
	private PagamentoTask payment;
	private EnvioTask shipping;
	private verificationPoint verificationPoint;
	
	@Before
	public void setUp() {
		report.startTest("Teste - Realizar uma compra com sucesso.");
		driver 	= drivers.getFirefoxDriver();
		add 	= new AdicionaItemaoCarrinhoTask(driver);
		address = new EnderecoTask(driver);
		newUser = new NovoUsuarioTask(driver);
		payment = new PagamentoTask(driver);
		shipping = new EnvioTask(driver);
		verificationPoint = new verificationPoint(driver);
	}
	@Test
	public void testMain() {
		driver.get("http://automationpractice.com/index.php?");
		driver.manage().window().maximize();
		report.log(Status.INFO, "O website foi carregado.", screenshot.capture(driver));
		add.addProductToCart();
		String name = add.productName();
		add.checkout();
		verificationPoint.checkProduct(name);
		String total = add.total();
		add.checkoutSummary();
		newUser.newAccount("daniel.lucas@frelo.com");
		String addressAccount = "Rua Teste, 732";
		String cityAccount = "Sapucaia do Sul";
		newUser.personalInformation("Daniel", "Lucas dos Santos", "password", addressAccount, cityAccount, "90765", "1589652389");
		newUser.submitAccount();
		verificationPoint.checkAddress(addressAccount,cityAccount);
		address.proceed();
		shipping.agreeTerms();
		shipping.proceed();
		verificationPoint.checkTotal(total);
		payment.payByBankWire();
		payment.proceed();
		verificationPoint.checkOrder();
		}
	@After
	public void tearDown() {
		driver.quit();
	}
}