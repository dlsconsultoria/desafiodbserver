package tasks;

import org.openqa.selenium.WebDriver;
import appobjects.PagamentoAppObject;

public class PagamentoTask {
	private PagamentoAppObject payment;
	
	public PagamentoTask(WebDriver driver){
		this.payment = new PagamentoAppObject(driver);
	}
	public void payByBankWire() {
		this.payment.payByBankWire().click();
	}
	public void proceed() {
		this.payment.proceed().click();
	}
}