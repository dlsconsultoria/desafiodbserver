package tasks;

import org.openqa.selenium.WebDriver;
import appobjects.AdicionaProdutoAppObject;

public class AdicionaItemaoCarrinhoTask {
	private AdicionaProdutoAppObject product;
	
	public AdicionaItemaoCarrinhoTask(WebDriver driver){
		this.product = new AdicionaProdutoAppObject(driver);
	}
	public void addProductToCart() {
		product.addToCart().click();
	}
	public String total() {
		return product.total().getText();//$27.00
	}
	public void checkout() {
		product.proceedToCheckout().click();
	}
	public void checkoutSummary() {
		product.proceedToCheckoutSummary().click();
	}
	public String productName() {
		return product.productNameCheck().getText();
	}
}