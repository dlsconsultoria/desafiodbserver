package tasks;

import org.openqa.selenium.WebDriver;
import appobjects.EnderecoAppObject;

public class EnderecoTask {
	private EnderecoAppObject address;
	
	public EnderecoTask(WebDriver driver){
		this.address = new EnderecoAppObject(driver);
	}
	public void proceed() {
		this.address.proceed().click();
	}
}