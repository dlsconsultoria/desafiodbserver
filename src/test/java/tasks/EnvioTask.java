package tasks;

import org.openqa.selenium.WebDriver;
import appobjects.EnvioAppObject;

public class EnvioTask {
	private EnvioAppObject shipping;
	
	public EnvioTask(WebDriver driver){
		this.shipping = new EnvioAppObject(driver);
	}
	public void agreeTerms() {
		this.shipping.check().click();
	}
	public void proceed() {
		this.shipping.proceed().click();
	}
}